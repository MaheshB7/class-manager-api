package com.demo.classmanagerapi.services;

import com.demo.classmanagerapi.entities.ClassAccount;
import com.demo.classmanagerapi.repositories.ClassAccountRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassAccountService {
    
    @Autowired
    private ClassAccountRepository classAccountRepository;

    /**
     * Creates new class account
     * @param classAccount Class to be created
     * @return Newly created account
     */
    public ClassAccount createClassAccount(ClassAccount classAccount) {
        try {
            return classAccountRepository.insert(classAccount);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
