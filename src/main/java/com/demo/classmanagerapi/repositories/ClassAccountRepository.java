package com.demo.classmanagerapi.repositories;

import com.demo.classmanagerapi.entities.ClassAccount;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClassAccountRepository extends MongoRepository<ClassAccount, String>{
    
}
