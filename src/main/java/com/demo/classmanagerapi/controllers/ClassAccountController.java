package com.demo.classmanagerapi.controllers;

import com.demo.classmanagerapi.entities.ClassAccount;
import com.demo.classmanagerapi.services.ClassAccountService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/class-account")
public class ClassAccountController {
    
    @Autowired
    private ClassAccountService accountService;

    /**
     * Creates new class account
     * @param account Class to be created
     * @return Newly created account
     */
    @PostMapping("/")
    public ResponseEntity<ClassAccount> createAccount(@RequestBody ClassAccount account) {
        ClassAccount classAccount;
        ResponseEntity<ClassAccount> response;
        try {
            classAccount = this.accountService.createClassAccount(account);
            response = new ResponseEntity<ClassAccount>(classAccount, HttpStatus.OK);
        } catch (Exception e) {
            response = new ResponseEntity<ClassAccount>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }
    
}
