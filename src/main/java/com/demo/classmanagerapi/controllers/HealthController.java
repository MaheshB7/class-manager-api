package com.demo.classmanagerapi.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health")
public class HealthController {

	@GetMapping("/")
	public Object healthCheck() {
		try {
			return "Application is running fine";
		} catch (Exception e) {
			e.printStackTrace();
			return "Application is down";
		}
	}
}
