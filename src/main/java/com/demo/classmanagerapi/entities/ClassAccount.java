package com.demo.classmanagerapi.entities;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document(collation = "class_account")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassAccount {
    @Id
    private String id;
    private String className;
    private String location;
    private String adminUserName;
    private String email;
    private String contactNumber;
    private String password;
    private Date createdDate;
    private Date updatedDate;
    private boolean isActive;
}